#include <stdio.h>
#include <stdint.h>
#include <netinet/in.h>

uint32_t readfile(const char* filename) {
	uint8_t buffer[4];
    FILE* file = fopen(filename, "rb");

    fread(buffer, sizeof(uint8_t), 4, file);

    fclose(file);
	return ntohl(*reinterpret_cast<uint32_t*>(buffer));
}

int main(int argc, char* argv[]){
	uint32_t num1;
	uint32_t num2;
	num1 = readfile(argv[1]);
	num2 = readfile(argv[2]);

	printf("%d(0x%x) + %d(0x%x) = %d(0x%x)\n", num1, num1, num2, num2, num1+num2, num1+num2);
}
